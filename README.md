# Salt Master in Docker

[![pipeline status](https://gitlab.com/4x0v7/salt-master-docker/badges/master/pipeline.svg)](https://gitlab.com/4x0v7/salt-master-docker/commits/master)

Build and bring up the containers

---

```shell
$ docker-compose -d up
Creating network "salt-master-docker_salt-net" with the default driver
Creating salt-master ... done
Creating salt-minion ... done
```

Shell into the master

---

```shell
$ docker-compose exec salt-master bash
[root@salt salt]# █
```

List keys

---

```shell
$ salt-key --list all
Accepted Keys:
Denied Keys:
Unaccepted Keys:
salt-minion
Rejected Keys:
```

Print keys

---

```shell
salt-key --print-all
```

Accept the salt-minion key

```shell
salt-key --accept salt-minion -y
```

or just

```shell
salt-key -A
```

## Test some commands

```shell
$ salt '*' test.ping
salt-minion:
    True
$ salt '*' grains.items
```
